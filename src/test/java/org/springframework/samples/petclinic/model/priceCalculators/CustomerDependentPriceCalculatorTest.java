package org.springframework.samples.petclinic.model.priceCalculators;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.model.UserType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerDependentPriceCalculatorTest {


    public CustomerDependentPriceCalculator customerDependentPriceCalculator = new CustomerDependentPriceCalculator();
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void calcPriceTestWithNoRareAndNoInfantAndNotGoldUser(){
       Pet cat = Mockito.mock(Pet.class);
       PetType type = Mockito.mock(PetType.class);
       when(type.getRare()).thenReturn(false);
       when(cat.getType()).thenReturn(type);
       DateTime birthDate = new DateTime();
       birthDate = birthDate.minusYears(10);
       when(cat.getBirthDate()).thenReturn(birthDate.toDate());
       List<Pet> pets = new ArrayList<Pet>();
       pets.add(cat);
       double baseCharge = 100;
       double basePricePerPet = 1000;
       UserType userType = UserType.SILVER;
       double totalPrice = this.customerDependentPriceCalculator.calcPrice(pets, baseCharge, basePricePerPet, userType);
       assertTrue(1000 == totalPrice);
    }



    @Test
    public void calcPriceTestWithRareWithInfantAndNotGoldUserDiscountCounterTrue(){
        List<Pet> pets = new ArrayList<Pet>();
        Pet cat = Mockito.mock(Pet.class);
        PetType type = Mockito.mock(PetType.class);
        when(type.getRare()).thenReturn(true);
        when(cat.getType()).thenReturn(type);
        DateTime birthDate = new DateTime();
        birthDate = birthDate.minusYears(2);
        when(cat.getBirthDate()).thenReturn(birthDate.toDate());
        Pet dog = Mockito.mock(Pet.class);
        when(dog.getType()).thenReturn(type);
        when(dog.getBirthDate()).thenReturn(birthDate.toDate());
        Pet wolf = Mockito.mock(Pet.class);
        when(wolf.getType()).thenReturn(type);
        when(wolf.getBirthDate()).thenReturn(birthDate.toDate());
        Pet snake = Mockito.mock(Pet.class);
        when(snake.getType()).thenReturn(type);
        when(snake.getBirthDate()).thenReturn(birthDate.toDate());
        Pet lion = Mockito.mock(Pet.class);
        when(lion.getType()).thenReturn(type);
        when(lion.getBirthDate()).thenReturn(birthDate.toDate());
        pets.add(cat);
        pets.add(dog);
        pets.add(wolf);
        pets.add(snake);
        pets.add(lion);
        double baseCharge = 100;
        double basePricePerPet = 1000;
        UserType userType = UserType.SILVER;
        double totalPrice = this.customerDependentPriceCalculator.calcPrice(pets, baseCharge, basePricePerPet, userType);
        assertTrue(7650 == totalPrice);
    }

    @Test
    public void calcPriceTestNoRareNotInfantAndGoldUser(){
        Pet cat = Mockito.mock(Pet.class);
        PetType type = Mockito.mock(PetType.class);
        when(type.getRare()).thenReturn(false);
        when(cat.getType()).thenReturn(type);
        DateTime birthDate = new DateTime();
        birthDate = birthDate.minusYears(10);
        when(cat.getBirthDate()).thenReturn(birthDate.toDate());
        List<Pet> pets = new ArrayList<Pet>();
        pets.add(cat);
        double baseCharge = 100;
        double basePricePerPet = 1000;
        UserType userType = UserType.GOLD;
        double totalPrice = this.customerDependentPriceCalculator.calcPrice(pets, baseCharge, basePricePerPet, userType);
        assertTrue(900 == totalPrice);

    }

    @Test
    public void calcPriceTestWithRareWithInfantAndNewUserDiscountCounterTrue(){
        List<Pet> pets = new ArrayList<Pet>();
        Pet cat = Mockito.mock(Pet.class);
        PetType type = Mockito.mock(PetType.class);
        when(type.getRare()).thenReturn(true);
        when(cat.getType()).thenReturn(type);
        DateTime birthDate = new DateTime();
        birthDate = birthDate.minusYears(2);
        when(cat.getBirthDate()).thenReturn(birthDate.toDate());
        Pet dog = Mockito.mock(Pet.class);
        when(dog.getType()).thenReturn(type);
        when(dog.getBirthDate()).thenReturn(birthDate.toDate());
        Pet wolf = Mockito.mock(Pet.class);
        when(wolf.getType()).thenReturn(type);
        when(wolf.getBirthDate()).thenReturn(birthDate.toDate());
        Pet snake = Mockito.mock(Pet.class);
        when(snake.getType()).thenReturn(type);
        when(snake.getBirthDate()).thenReturn(birthDate.toDate());
        Pet lion = Mockito.mock(Pet.class);
        when(lion.getType()).thenReturn(type);
        when(lion.getBirthDate()).thenReturn(birthDate.toDate());
        pets.add(cat);
        pets.add(dog);
        pets.add(wolf);
        pets.add(snake);
        pets.add(lion);
        double baseCharge = 100;
        double basePricePerPet = 1000;
        UserType userType = UserType.NEW;
        double totalPrice = this.customerDependentPriceCalculator.calcPrice(pets, baseCharge, basePricePerPet, userType);
        assertTrue(8080 == totalPrice);
    }

    @Test
    public void calcPriceTestWithRareWithAndWithoutInfantDiscountCounterTrue(){
        List<Pet> pets = new ArrayList<Pet>();
        Pet cat = Mockito.mock(Pet.class);
        PetType type = Mockito.mock(PetType.class);
        when(type.getRare()).thenReturn(true);
        when(cat.getType()).thenReturn(type);
        DateTime infantBirthDate = new DateTime();
        infantBirthDate = infantBirthDate.minusYears(2);
        DateTime notInfantBirthDate = new DateTime();
        notInfantBirthDate = notInfantBirthDate.minusYears(10);
        when(cat.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet dog = Mockito.mock(Pet.class);
        when(dog.getType()).thenReturn(type);
        when(dog.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet wolf = Mockito.mock(Pet.class);
        when(wolf.getType()).thenReturn(type);
        when(wolf.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet snake = Mockito.mock(Pet.class);
        when(snake.getType()).thenReturn(type);
        when(snake.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet lion = Mockito.mock(Pet.class);
        when(lion.getType()).thenReturn(type);
        when(lion.getBirthDate()).thenReturn(notInfantBirthDate.toDate());
        Pet tiger = Mockito.mock(Pet.class);
        when(tiger.getType()).thenReturn(type);
        when(tiger.getBirthDate()).thenReturn(notInfantBirthDate.toDate());

        pets.add(cat);
        pets.add(dog);
        pets.add(wolf);
        pets.add(snake);
        pets.add(lion);
        pets.add(tiger);
        double baseCharge = 100;
        double basePricePerPet = 1000;
        UserType userType = UserType.NEW;
        double totalPrice = this.customerDependentPriceCalculator.calcPrice(pets, baseCharge, basePricePerPet, userType);
        assertTrue(8764 == totalPrice);
    }

    @Test
    public void calcPriceTestWithAndWithoutInfantDiscountCounterTrue(){
        List<Pet> pets = new ArrayList<Pet>();
        Pet cat = Mockito.mock(Pet.class);
        PetType rareType = Mockito.mock(PetType.class);
        PetType notRareType = Mockito.mock(PetType.class);
        when(rareType.getRare()).thenReturn(true);
        when(notRareType.getRare()).thenReturn(false);
        when(cat.getType()).thenReturn(rareType);
        DateTime infantBirthDate = new DateTime();
        infantBirthDate = infantBirthDate.minusYears(2);
        DateTime notInfantBirthDate = new DateTime();
        notInfantBirthDate = notInfantBirthDate.minusYears(10);
        when(cat.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet dog = Mockito.mock(Pet.class);
        when(dog.getType()).thenReturn(rareType);
        when(dog.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet wolf = Mockito.mock(Pet.class);
        when(wolf.getType()).thenReturn(rareType);
        when(wolf.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet snake = Mockito.mock(Pet.class);
        when(snake.getType()).thenReturn(rareType);
        when(snake.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet lion = Mockito.mock(Pet.class);
        when(lion.getType()).thenReturn(notRareType);
        when(lion.getBirthDate()).thenReturn(infantBirthDate.toDate());
        Pet tiger = Mockito.mock(Pet.class);
        when(tiger.getType()).thenReturn(notRareType);
        when(tiger.getBirthDate()).thenReturn(notInfantBirthDate.toDate());

        pets.add(cat);
        pets.add(dog);
        pets.add(wolf);
        pets.add(snake);
        pets.add(lion);
        pets.add(tiger);
        double baseCharge = 100;
        double basePricePerPet = 1000;
        UserType userType = UserType.SILVER;
        double totalPrice = this.customerDependentPriceCalculator.calcPrice(pets, baseCharge, basePricePerPet, userType);
        assertTrue(8118 == totalPrice);
    }
}
