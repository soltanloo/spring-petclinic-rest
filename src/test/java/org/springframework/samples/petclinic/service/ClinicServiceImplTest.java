package org.springframework.samples.petclinic.service;

import com.github.mryf323.tractatus.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.repository.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ClinicServiceImplTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    PetRepository petRepository = Mockito.mock(PetRepository.class);
    @Mock
    VetRepository vetRepository = Mockito.mock(VetRepository.class);
    @Mock
    OwnerRepository ownerRepository = Mockito.mock(OwnerRepository.class);
    @Mock
    VisitRepository visitRepository = Mockito.mock(VisitRepository.class);
    @Mock
    SpecialtyRepository specialtyRepository = Mockito.mock(SpecialtyRepository.class);
    @Mock
    PetTypeRepository petTypeRepository = Mockito.mock(PetTypeRepository.class);
    @InjectMocks
    ClinicService clinicService = new ClinicServiceImpl(petRepository,vetRepository,ownerRepository,visitRepository,specialtyRepository,petTypeRepository);

    @CACC(
        predicate = "a = len(pets) > 0",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        },
        predicateValue = false
    )

    @CACC(
        predicate = "a = notVisited.size() > 0",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        },
        predicateValue = false
    )
    @NearFalsePoint(
        predicate = "a = len(pets) > 0",
        cnf = "a",
        clause = 'a',
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )
    @NearFalsePoint(
        predicate = "a = notVisited.size() > 0",
        cnf = "a",
        clause = 'a',
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )
    @NearFalsePoint(
        predicate = "a = last.isPresent == True",
        cnf = "a",
        clause = 'a',
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
    }
    )
    @CACC(
        predicate = "a = last.isPresent == True",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        },predicateValue = false
    )
    @ClauseCoverage(
        predicate = "a = i < len(pets)",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )

    @ClauseCoverage(
        predicate = "a = last.isPresent == True",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )

    @ClauseCoverage(
        predicate = "b = notVisited.size() > 0",
        valuations = {
            @Valuation(clause = 'b', valuation = false)
        }
    )
    @Test
    public void visitOwnerPetsNoLoop() {
        Collection<Pet> petsList = new ArrayList<Pet>();
        Owner owner = new Owner();
        when(petRepository.findByOwner(owner)).thenReturn(petsList);
        clinicService.visitOwnerPets(owner);

    }
    @UniqueTruePoint(
        predicate = "a = last.isPresent == True",
        cnf = "a",
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @CACC(
        predicate = "a = last.isPresent == True",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        },predicateValue = true
    )
    @ClauseCoverage(
        predicate = "a = i < len(pets)",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @CACC(
        predicate = "a = len(pets) > 0",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        },
        predicateValue = true
    )
    @UniqueTruePoint(
        predicate = "a = len(pets) > 0",
        cnf = "a",
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @ClauseCoverage(
        predicate = "c = last.isPresent()",
        valuations = {
            @Valuation(clause = 'c', valuation = true)
}
    )
    @ClauseCoverage(
        predicate = "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = true)
    }
    )

    @Test
    public void visitOwnerPetsLoopWithFirstIfTrueAndSecondIfTrue() {
        Collection<Pet> petsList = new ArrayList<Pet>();
        Pet pet = new Pet();
        pet.setName("cat");
        PetType petType = new PetType();
        petType.setName("mamal");
        pet.setType(petType);
        LocalDate date = LocalDate.now().minusYears(4);
        java.util.Date birthDate = java.sql.Date.valueOf(date);
        pet.setBirthDate(birthDate);
        petsList.add(pet);
        Owner owner = new Owner();
        when(petRepository.findByOwner(owner)).thenReturn(petsList);
        Visit last = new Visit();
        last.setDate(birthDate);
        pet.addVisit(last);
        Collection<Vet> vetList = new ArrayList<Vet>();
        Vet vet = new Vet();
        Specialty specialty = Mockito.mock(Specialty.class);
        when(specialty.canCure(petType)).thenReturn(true);
        vet.addSpecialty(specialty);
        vetList.add(vet);
        when(vetRepository.findAll()).thenReturn(vetList);
        clinicService.visitOwnerPets(owner);
        Mockito.verify(visitRepository).save(any(Visit.class));
    }

    @ClauseCoverage(
        predicate = "a = notVisited.size() > 0",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @UniqueTruePoint(
        predicate = "a = notVisited.size() > 0",
        cnf = "a = notVisited.size() > 0",
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @CACC(
        predicate = "a = notVisited.size() > 0",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        },
        predicateValue = true
    )
    @Test
    public void visitOwnerPetsLoopWithFirstIfFalseAndLastIfTrue(){
        Collection<Pet> petsList = new ArrayList<Pet>();
        Pet pet = Mockito.mock(Pet.class);
        Visit visit = Mockito.mock(Visit.class);
        when(pet.getLastVisit()).thenReturn(Optional.ofNullable(null));
        PetType petType = Mockito.mock(PetType.class);
        when(pet.getType()).thenReturn(petType);
        petsList.add(pet);
        Owner owner = new Owner();
        when(petRepository.findByOwner(owner)).thenReturn(petsList);
        Collection<Vet> vetList = new ArrayList<Vet>();
        Vet vet = new Vet();
        vetList.add(vet);
        Specialty specialty = Mockito.mock(Specialty.class);
        when(specialty.canCure(petType)).thenReturn(false);
        vet.addSpecialty(specialty);
        when(vetRepository.findAll()).thenReturn(vetList);
        exception.expect(ClinicServiceImpl.VisitException.class);
        clinicService.visitOwnerPets(owner);

    }

    @ClauseCoverage(
        predicate = "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = false)
        }
    )
    @NearFalsePoint(
        predicate = "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        cnf =  "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        implicant = "cd",
        clause = 'd',
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = false)
        }
    )
    @CACC(
        predicate = "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        majorClause = 'd',
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = false)
        },
        predicateValue = false
    )
    @Test
    public void visitOwnerPetsLoopWithFirstIfTrueAndSecondIfFalseByAllClause() {
        Collection<Pet> petsList = new ArrayList<Pet>();
        Pet pet = new Pet();
        pet.setName("cat");
        PetType petType = new PetType();
        petType.setName("mamal");
        pet.setType(petType);
        LocalDate date = LocalDate.now().minusDays(1);
        java.util.Date birthDate = java.sql.Date.valueOf(date);
        pet.setBirthDate(birthDate);
        petsList.add(pet);
        Owner owner = new Owner();
        when(petRepository.findByOwner(owner)).thenReturn(petsList);
        Visit last = new Visit();
        last.setDate(birthDate);
        pet.addVisit(last);
        Collection<Vet> vetList = new ArrayList<Vet>();
        Vet vet = new Vet();
        Specialty specialty = Mockito.mock(Specialty.class);
        when(specialty.canCure(petType)).thenReturn(true);

        vet.addSpecialty(specialty);
        vetList.add(vet);
        when(vetRepository.findAll()).thenReturn(vetList);
        clinicService.visitOwnerPets(owner);
        Mockito.verify(visitRepository, never()).save(any(Visit.class));
    }

    @NearFalsePoint(
        predicate = "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        cnf =  "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        implicant = "cd",
        clause = 'b',
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = false)
        }
    )
    @CACC(
        predicate = "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        majorClause = 'b',
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = false)
        },predicateValue = false
    )
    @Test
    public void visitOwnerPetsLoopWithFirstIfTrueAndSecondIfFalse() {
        Collection<Pet> petsList = new ArrayList<Pet>();
        Pet pet = new Pet();
        pet.setName("cat");
        PetType petType = new PetType();
        petType.setName("mamal");
        pet.setType(petType);
        LocalDate date = LocalDate.now().minusYears(4);
        LocalDate vdate = LocalDate.now().minusDays(1);
        java.util.Date birthDate = java.sql.Date.valueOf(date);
        java.util.Date visitDate = java.sql.Date.valueOf(vdate);
        pet.setBirthDate(birthDate);
        petsList.add(pet);
        Owner owner = new Owner();
        when(petRepository.findByOwner(owner)).thenReturn(petsList);
        Visit last = new Visit();
        last.setDate(visitDate);
        pet.addVisit(last);
        Collection<Vet> vetList = new ArrayList<Vet>();
        Vet vet = new Vet();
        Specialty specialty = Mockito.mock(Specialty.class);
        when(specialty.canCure(petType)).thenReturn(true);
        vet.addSpecialty(specialty);
        vetList.add(vet);
        when(vetRepository.findAll()).thenReturn(vetList);
        clinicService.visitOwnerPets(owner);
        Mockito.verify(visitRepository,times(0)).save(any(Visit.class));
    }
    @UniqueTruePoint(
        predicate = "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        cnf =  "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        implicant = "cd",
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = true)
        }
    )
    @CACC(
        predicate = "(a = age>3 && b = daysFromLastVisit>364) || (c = age<=3 && d = daysFromLastVisit>182)",
        majorClause = 'd',//redundant with c being major clause
        valuations = {
            @Valuation(clause = 'a', valuation = false),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = true)
        },predicateValue = true
    )
    @Test
    public void visitOwnerPetsLoopWithFirstIfTrueAndSecondIfTrueWithSecondClause() {
        Collection<Pet> petsList = new ArrayList<Pet>();
        Pet pet = new Pet();
        pet.setName("cat");
        PetType petType = new PetType();
        petType.setName("mamal");
        pet.setType(petType);
        LocalDate date = LocalDate.now().minusYears(2);
        LocalDate vdate = LocalDate.now().minusDays(183);
        java.util.Date birthDate = java.sql.Date.valueOf(date);
        java.util.Date visitDate = java.sql.Date.valueOf(vdate);
        pet.setBirthDate(birthDate);
        petsList.add(pet);
        Owner owner = new Owner();
        when(petRepository.findByOwner(owner)).thenReturn(petsList);
        Visit last = new Visit();
        last.setDate(visitDate);
        pet.addVisit(last);
        Collection<Vet> vetList = new ArrayList<Vet>();
        Vet vet = new Vet();
        Specialty specialty = Mockito.mock(Specialty.class);
        when(specialty.canCure(petType)).thenReturn(true);
        vet.addSpecialty(specialty);
        vetList.add(vet);
        when(vetRepository.findAll()).thenReturn(vetList);
        clinicService.visitOwnerPets(owner);
        Mockito.verify(visitRepository).save(any(Visit.class));
    }
}
