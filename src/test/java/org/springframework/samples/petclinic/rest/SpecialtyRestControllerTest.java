package org.springframework.samples.petclinic.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.samples.petclinic.model.Specialty;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SpecialtyRestController.class)
public class SpecialtyRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClinicService clinicService;

    @Autowired
    private ObjectMapper objectMapper;

    private List<Specialty> specialties;

    @Before
    public void setUp() throws Exception {
        specialties = new ArrayList<Specialty>();

        Specialty specialty = new Specialty();
        specialty.setId(1);
        specialty.setName("AAA");
        specialties.add(specialty);

        specialty = new Specialty();
        specialty.setId(2);
        specialty.setName("BBB");
        specialties.add(specialty);

        specialty = new Specialty();
        specialty.setId(3);
        specialty.setName("CCC");
        specialties.add(specialty);

        specialty = new Specialty();
        specialty.setId(3);
        specialty.setName("DDD");
        specialties.add(specialty);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void updatingSpecialtyWithoutAdminRoleCausesIsUnauthorizedError() throws Exception {
        this.mockMvc
            .perform(put("/api/specialties/{specialtyId}", 1).with(csrf()))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "VET_ADMIN")
    public void updatingSpecialtyWhichIsNotDefinedCausesNotFoundError() throws Exception {
        when(clinicService.findSpecialtyById(1)).thenReturn(null);
        this.mockMvc
            .perform(put("/api/specialties/{specialtyId}", 1)
                .with(csrf())
                .content(objectMapper.writeValueAsString(specialties.get(0)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            )
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "VET_ADMIN")
    public void updatingSpecialtyWithEmptyBodyCausesBadRequest() throws Exception {
        this.mockMvc
            .perform(
                put("/api/specialties/{specialtyId}", 1)
                    .with(csrf())
                    .content("")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            )
            .andDo(print())
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = "VET_ADMIN")
    public void updatingSpecialtyWithNonJSONBodyCausesBadRequest() throws Exception {
        this.mockMvc
            .perform(
                put("/api/specialties/{specialtyId}", 1)
                    .with(csrf())
                    .content("Blah")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            )
            .andDo(print())
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = "VET_ADMIN")
    public void updatingSpecialtyNameWithAppropriateDataSucceeds() throws Exception {
        when(clinicService.findSpecialtyById(3)).thenReturn(specialties.get(2));
        this.mockMvc
            .perform(
                put("/api/specialties/{specialtyId}", 3)
                    .with(csrf())
                    .content(objectMapper.writeValueAsString(specialties.get(3)))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            )
            .andDo(print())
            .andExpect(status().isNoContent());

        verify(clinicService, times(1)).findSpecialtyById(3);
        verify(clinicService, times(1)).saveSpecialty(specialties.get(2));
    }
}
